package me.learn.springauthclient;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.reactive.result.view.RedirectView;

@Controller
public class AuthController {

    @GetMapping("logged-out")
    public RedirectView logout() {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/");
        return redirectView;
    }

    @GetMapping("user")
    @ResponseBody
    public ResponseEntity<?> user(@AuthenticationPrincipal OidcUser oidcUser) {
        if(null==oidcUser){
            return ResponseEntity.badRequest().build();
        }else {
            System.out.println(oidcUser.toString());
            return ResponseEntity.ok(oidcUser.toString());
        }


    }
}
